package com.training.accountmgmt.model;

public class UserInfo {
	 private int id; 
	  private String name; 
	  private String username; 
	  private String email; 
	  private Address address; 
	  private String phone; 
	  private String website; 
	  private Company company; 

	  public int getId(){
	  	return id; 
	  }
	  public void setId(int input){
	  	 this.id = input;
	  }
	  public String getName(){
	  	return name; 
	  }
	  public void setName(String input){
	  	 this.name = input;
	  }
	  public String getUsername(){
	  	return username; 
	  }
	  public void setUsername(String input){
	  	 this.username = input;
	  }
	  public String getEmail(){
	  	return email; 
	  }
	  public void setEmail(String input){
	  	 this.email = input;
	  }
	  public Address getAddress(){
	  	return address; 
	  }
	  public void setAddress(Address input){
	  	 this.address = input;
	  }
	  public String getPhone(){
	  	return phone; 
	  }
	  public void setPhone(String input){
	  	 this.phone = input;
	  }
	  public String getWebsite(){
	  	return website; 
	  }
	  public void setWebsite(String input){
	  	 this.website = input;
	  }
	  public Company getCompany(){
	  	return company; 
	  }
	  public void setCompany(Company input){
	  	 this.company = input;
	  }
}
