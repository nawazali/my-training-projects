package com.training.accountmgmt.test;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hibernate.service.spi.InjectService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.MockitoAnnotations.Mock;
import org.mockito.internal.util.MockitoLogger;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import com.training.accountmgmt.controller.AccountMgmtController;
import com.training.accountmgmt.controller.JsonModel;
import com.training.accountmgmt.controller.Value;
import com.training.accountmgmt.model.AccountInfo;
@RunWith(MockitoJUnitRunner.class)
public class TestAccountMgmt {
	
	private MockMvc mockMvc;
	@InjectMocks
	AccountMgmtController acctMgmtController;
	
	String response;
	JsonModel jsonmodel;
	
	
	@Before
	public void setUp() {
		jsonmodel=new JsonModel();
		jsonmodel.setType("success");
		Value value=new Value();
		jsonmodel.setValue(value);

	}
	
	
	@Test
	public void testGetAcctDtlsWithValidAccountNo()  {
		String url="http://gturnquist-quoters.cfapps.io/api/random";
		JsonModel resultjsonModel=acctMgmtController.getAcctDtls(url);
		assertEquals(jsonmodel.getType(),resultjsonModel.getType());
	}
	@Test(expected=NullPointerException.class)
	public void testGetAcctDtlsWithValidAccountNo1() throws Exception {
		String url="";
		JsonModel resultjsonModel=acctMgmtController.getAcctDtls(url);
		
	}
	
	
	

}
