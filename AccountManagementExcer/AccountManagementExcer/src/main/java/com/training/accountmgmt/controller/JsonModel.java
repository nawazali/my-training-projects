package com.training.accountmgmt.controller;

public class JsonModel {
private String type;
private Value value;
public Value getValue() {
	return value;
}

public void setValue(Value value) {
	this.value = value;
}

public String getType() {
	return type;
}

public void setType(String type) {
	this.type = type;
}

@Override
public String toString() {
	return "JsonModel [type=" + type + ", value=" + value + "]";
}



}
