package com.training.accountmgmt.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

@Controller
public class AccountMgmtController {
	
	

	@RequestMapping(value="/account")
	public JsonModel getAcctDtls(String url) {
		RestTemplate restTemplate = new RestTemplate();
		if(url==null || url.isEmpty())
			throw new NullPointerException();
		
        String quote = restTemplate.getForObject(url, String.class);
       
        Gson g = new Gson();
        JsonModel j=g.fromJson(quote, JsonModel.class);
        
        return j;
       }
}
