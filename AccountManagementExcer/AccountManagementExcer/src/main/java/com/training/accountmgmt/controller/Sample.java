package com.training.accountmgmt.controller;

import java.net.URI;
import java.util.Queue;

import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.training.accountmgmt.model.UserInfo;

public class Sample {

	public static void main(String[] args) {
		RestTemplate restTemplate = new RestTemplate();
		
        String quote = restTemplate.getForObject("http://jsonplaceholder.typicode.com/users/1", String.class);
        Gson g = new Gson();
        UserInfo j=g.fromJson(quote, UserInfo.class);
        System.out.println(quote.toString());
        System.out.println(j.toString());
        

		
		
	}

}
